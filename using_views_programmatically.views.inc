<?php
/**
 * Views automatically loads the MODULE.views.inc file,
 * but these could also reside in the .module file.
 * Either location is fine and it's really just a matter
 * of code organization.
 *
 * In all functions:
 * using_views_programatically
 * is the "hook".  Yes, it's ridiculously long.
 */

/**
 * Implements hook_views_query_alter()
 */
function using_views_programmatically_views_query_alter(&$view, &$query) {
  /**
   * Here's a few things you might want to enable 
   * to see what's in the arguments being passed
   */
  //dpm($view);
  //dpm(get_class_methods($view));
  //dpm($query);
  //dpm(get_class_methods($query));

  /*
  if ($view->name == 'query_alter') {
    $query->add_where_expression(0, 'users_node.uid = node.nid');
  } // if query_alter view
  */

} // using_views_programmatically_views_query_alter()

/**
 * Implements hook_views_post_render()
 */
function using_views_programmatically_views_post_render(&$view, &$output, &$cache) {
  /**
   * Here's a few things you might want to enable 
   * to see what's in the arguments being passed
   */
  //dpm($view);
  //dpm(get_class_methods($view));
  //dpm($output);
  //dpm($cache);

  /*
  if ($view->name == 'post_render_1') {
    // uids[0] is full match, uids[1] is sub-match
    if (preg_match_all('/--post_render-(\d+)--/', $output, $uids)) { 

      // initialize replacement array
      $replacements = array();

      // generate replacements
      foreach ($uids[1] as $key => $uid) {
        // setup the sub-view
        $view2 = views_get_view('post_render_2');
        $view2->set_display('block_1');

        // give each view a different pager id so they act independently
        $view2->display_handler->options['pager']['options']['id'] = $uid;

        $view2->set_arguments(array($uid));
        $view2->execute();

        // populate the replacement array
        $replacements[$uids[0][$key]] = $view2->render();
      } // for

      $output = strtr($output, $replacements);
    } // if preg_match_all
  } // if post_render_1 view
  */

} // using_views_programmatically_views_post_render()

